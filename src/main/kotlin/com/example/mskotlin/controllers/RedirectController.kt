package com.example.mskotlin.controllers

import com.example.mskotlin.services.KeyMapperService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletResponse

@RestController
@RequestMapping("/{key}")
class RedirectController {

    @Autowired
    lateinit var keyMapperService: KeyMapperService

    @RequestMapping
    fun redirect(@PathVariable("key") key: String, response: HttpServletResponse) {

        val result = keyMapperService.getLink(key)
        when (result) {
            is KeyMapperService.Get.Link -> {
                response.setHeader(HEADER_NAME, result.link)
                response.status = 302
            }
            is KeyMapperService.Get.NotFound -> {
                response.status = 404
            }
        }

    }

    companion object {
        private val HEADER_NAME = "Location"
    }
}