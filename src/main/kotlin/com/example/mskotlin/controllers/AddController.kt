package com.example.mskotlin.controllers

import com.example.mskotlin.services.KeyMapperService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/add")
class AddController {
    @Autowired
    lateinit var keyMapperService: KeyMapperService

    @PostMapping
    fun add(@RequestBody request: AddRequest) =
            ResponseEntity.ok(AddResponse(request.link, keyMapperService.add(request.link)))

    data class AddRequest(val link: String)
    data class AddResponse(val link: String, val key: String)
}
