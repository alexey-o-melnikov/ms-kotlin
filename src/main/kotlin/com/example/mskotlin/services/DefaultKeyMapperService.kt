package com.example.mskotlin.services

import com.example.mskotlin.model.Link
import com.example.mskotlin.model.repositories.LinkRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class DefaultKeyMapperService : KeyMapperService {

    @Autowired
    lateinit var converter: KeyConverterService

    @Autowired
    lateinit var linkRepository: LinkRepository

    @Transactional
    override fun add(link: String): String =
            converter.idToKey(linkRepository.save(Link(link)).id)

    override fun getLink(key: String): KeyMapperService.Get {
        val result = linkRepository.findOne(converter.keyToId(key))
        return if (result.isPresent) {
            KeyMapperService.Get.Link(result.get().text)
        } else {
            KeyMapperService.Get.NotFound(key)
        }
    }
}
