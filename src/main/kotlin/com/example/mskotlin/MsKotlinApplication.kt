package com.example.mskotlin

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class MsKotlinApplication

fun main(args: Array<String>) {
    SpringApplication.run(MsKotlinApplication::class.java, *args)
}