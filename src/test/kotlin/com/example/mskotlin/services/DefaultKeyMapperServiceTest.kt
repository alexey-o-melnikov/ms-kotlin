package com.example.mskotlin.services

import com.example.mskotlin.model.Link
import com.example.mskotlin.model.repositories.LinkRepository
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class DefaultKeyMapperServiceTest {

    @InjectMocks
    val service: KeyMapperService = DefaultKeyMapperService()

    @Mock
    lateinit var keyConverterService: KeyConverterService

    @Mock
    lateinit var repository: LinkRepository

    private val KEY = "vVyvyVIYVYJVYIIiv"
    private val LINK_A = "https://www.google.ru"
    private val LINK_B = "https://www.ya.ru"

    private val KEY_A = "abc"
    private val KEY_B = "cde"
    private val ID_A = 10_000_000L
    private val ID_B = 10_000_000L + 1

    private val LINK_OBJ_A: Link = Link(LINK_A, ID_A)
    private val LINK_OBJ_B: Link = Link(LINK_B, ID_B)

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
        Mockito.`when`(keyConverterService.keyToId(KEY_A)).thenReturn(ID_A)
        Mockito.`when`(keyConverterService.idToKey(ID_A)).thenReturn(KEY_A)
        Mockito.`when`(keyConverterService.keyToId(KEY_B)).thenReturn(ID_B)
        Mockito.`when`(keyConverterService.idToKey(ID_B)).thenReturn(KEY_B)

        Mockito.`when`(repository.findOne(Mockito.anyObject())).thenReturn(Optional.empty())
        Mockito.`when`(repository.save(Link(LINK_A))).thenReturn(LINK_OBJ_A)
        Mockito.`when`(repository.save(Link(LINK_B))).thenReturn(LINK_OBJ_B)
        Mockito.`when`(repository.findOne(ID_A)).thenReturn(Optional.of(LINK_OBJ_A))
        Mockito.`when`(repository.findOne(ID_B)).thenReturn(Optional.of(LINK_OBJ_B))
    }

    @Test
    fun clientCanAddLinks() {
        val keyA = service.add(LINK_A)
        assertEquals(KeyMapperService.Get.Link(LINK_A), service.getLink(keyA))
        val keyB = service.add(LINK_B)
        assertEquals(KeyMapperService.Get.Link(LINK_B), service.getLink(keyB))
        assertNotEquals(keyA, keyB)
        println()
    }

    @Test
    fun clientCanNotTakeLinkIfKeyIsNotFoundInService() {
        assertEquals(KeyMapperService.Get.NotFound(KEY), service.getLink(KEY))
    }

}